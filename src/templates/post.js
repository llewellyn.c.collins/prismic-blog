import React from "react"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import SEO from "../components/seo"
import Layout from "../components/layout"

const Post = ({ data }) => {
    const {data: postData, first_publication_date: date} = data.prismicPost;
    const title = postData.title.text;
    const content = postData.content.html;
    const image = postData.image.localFile.childImageSharp.fluid;
    const imageAltText = postData.image.alt;
    return (
        <Layout>
            <SEO title={title}/>
            <section>
                <Img fluid={image} alt={imageAltText}/>
                <h1>{title}</h1>
                <h6>{date}</h6>
                <article>
                    <p dangerouslySetInnerHTML={{ __html: content }}/>
                </article>
            </section>
        </Layout>
    )
}

export default Post

export const pageQuery = graphql`
  query PostBySlug($uid: String!) { # Query the post with the uid passed in from gatsby-node.js
    prismicPost(uid: { eq: $uid }) {
      uid
      first_publication_date(formatString: "DD MMMM YYYY")
      data {
        title {
          text
        }
        content {
          html
        }
        image {
            alt
            localFile {
                childImageSharp {
                    fluid(maxWidth: 1200) {
                        ...GatsbyImageSharpFluid
                    }
                }
            }
        }
      }
    }
  }
`