import React from "react"
import { graphql, Link } from "gatsby"
import SEO from "../components/seo"
import Layout from "../components/layout"

const PostsPage = ({ data:queryData }) => {
    const posts = queryData.allPrismicPost.edges;
    return (
      <Layout>
        <SEO title="Posts" description="Page containing a list of all the posts"/>
        <section>
            <h1>Posts</h1>
            <article>
                <ul>
                    {
                        posts.map( ( post, index ) => {
                            const {data:postData, first_publication_date: date, uid} = post.node;
                            const title = postData.title.text;
                            const url = `/posts/${uid}`;
                            return (
                                <li key={index}>
                                    <Link to={url}>{title}</Link> - <span>{date}</span>
                                </li>
                            )
                        } )
                    }
                </ul>
            </article>
        </section>
      </Layout>
    )
}

export default PostsPage

export const blogQuery = graphql`
query PostsQuery {
    allPrismicPost(sort:{                       # Sort the posts
            fields: [last_publication_date],    # The fields to sort by
            order: DESC                         # The sort order
        }) {
        edges {
            node {
                id
                uid
                first_publication_date(formatString: "DD MMMM YYYY")
                data {
                    title {
                        text
                    }
                    content {
                        html
                    }
                    image {
                        alt
                        localFile {
                            childImageSharp {
                                fluid(maxWidth: 1200) {
                                    ...GatsbyImageSharpFluid
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
`