import React from "react"
import { Link } from "gatsby"
import Img from "gatsby-image"
import Layout from "../components/layout"
import SEO from "../components/seo"

const IndexPage = ( {data:queryData} ) => {
  const pageData = queryData.prismicHome.data;
  const title = pageData.title.text;
  const content = pageData.content.html;
  const image = pageData.image.localFile.childImageSharp.fluid;
  const imageAltText = pageData.image.alt;

  return (
  <Layout>
    {/* Use the default SEO component */}
    <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />
    {/* The home page layout */}
    <nav>
      <ul>
        <li><Link to="/posts">Posts</Link></li>
      </ul>
    </nav>
    <section>
      {/* Use the Gatsby Image component. This will display a fully optimised image to ensure fast page load times */}
      <Img fluid={image} alt={imageAltText}/>
      {/* Add the Title*/}
      <h1>{title}</h1>
      <article> 
        {/* Include teh home page content */}
        <p dangerouslySetInnerHTML={{ __html: content }}/>
      </article>
    </section>
  </Layout>
  )
}

export default IndexPage

export const homeQuery = graphql`
query homeQuery {
  prismicHome {
    data {
      title {
        text
      }
      content {
        html
      }
      image {
        alt
        localFile { # This will import the image as a local file
          childImageSharp { # Image Sharp is some awesome Gatsby sauce. It will import your fully optimized image at different resolutions
            fluid(maxWidth: 1200) {
                ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }
  }
}
`