const path = require("path")

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions;

  // Fetch all the posts
  const posts = await graphql(`
    {
      allPrismicPost {
        edges {
          node {
            uid
          }
        }
      }
    }
  `).catch((error)=>{
      console.error(error);
  });

  // Load the post template
  const template = path.resolve("src/templates/post.js")

  posts.data.allPrismicPost.edges.forEach(edge => {
    // Create a page for every post 
    createPage({
      path: `/posts/${edge.node.uid}`,
      component: template,
      context: {
        uid: edge.node.uid,
      },
    })
  })
}